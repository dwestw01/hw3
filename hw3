"""
HOMEWORK 3
Due 10/21/204

@authors:

David Westwood
Greg Kline


Psuedo-code:

class Application
    init
        create tk Frame
        call createWidgets
        create fig
    createWidgets
        creates sliders
        creates buttons
        creates Labels
        creates dropdown menu
    plot
        gets initial conditions from widgets 
        uses odeint to solve equation() for double pendulum 
        creates arrays for points for x1,y1 and x2,y2
        animates linkages using x1,y1 and x2,y2
        creates a plot for variable selected in dropdown menu
    init
        initializes variables for animation
    animate
        called in plot() to animate linkages
        returns line and time counter
    equation
        contains the differential equations for double pendulum 
        called in plot() to solve ODE given initial conditions

"""


import Tkinter as tk

import numpy as np
import matplotlib
matplotlib.use('TkAgg')

from numpy import sin, pi, cos

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import scipy.integrate as integrate

TITLE_FONT = ("Helvetica", 10)

class Application(tk.Frame):

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
 
        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.createWidgets()
        self.pack()
        self.font = ("Helvetica", 18, "bold")
        
        #Parameters for the simulation
        self.g = 9.81 
        
        
        self.fig = plt.figure()

    def createWidgets(self):

        # label and slider for selecting Mass 2
        self.label_m1 = tk.Label(self,text="Mass 1", font=TITLE_FONT).grid(column=0,row=1)
        self.m1 = tk.Scale(self, from_=0.10, to=10, resolution=0.1)
        self.m1.grid(column=0, row = 2)
        self.m1.set(1.0)
        # label and slider for selecting Mass 2
        self.label_m2 = tk.Label(self,text="Mass 2", font=TITLE_FONT).grid(column=1,row=1)
        self.m2 = tk.Scale(self, from_=0.10, to=10, resolution=0.1)
        self.m2.grid(column=1, row = 2)
        self.m2.set(1.0)

        #Create buttons                
        self.quit = tk.Button(self, text="Quit", command=self.quit).grid(column=2,row=4)
        self.plot = tk.Button(self, text="Plot", command=self.plot).grid(column=2,row=3)
            
        # Labels for Time and dt boxes and length boxes
        self.label_length1 = tk.Label(self, text="Length 1", font=TITLE_FONT).grid(column=0, row=3)
        self.label_length2 = tk.Label(self, text="Length 2", font=TITLE_FONT).grid(column=0, row=4)
        self.label_tinit = tk.Label(self, text="Time: ", font=TITLE_FONT).grid(column=0,row=5)
        self.label_tinit = tk.Label(self, text="dt: ", font=TITLE_FONT).grid(column=0,row=6)
        # entry box for Time              
        self.time = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.time).grid(column=1,row=5)
        self.time.set("10.0")
        # entry box for dt
        self.dt_entry = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.dt_entry).grid(column=1,row=6)
        self.dt_entry.set("0.05")

        # entry box for Length 1
        self.length1 = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.length1).grid(column=1, row=3)
        self.length1.set("1")

        # entry box for Length 2
        self.length2 = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.length2).grid(column=1, row=4)
        self.length2.set("1")
                
        # create option menu to select variable for plot
        self.label_variable = tk.Label(self, text="Variable for plot:\n 0 - th1 \n 1 - w1 \n 2 - th2 \n 3 - w2 ", font=TITLE_FONT).grid(column=3,row=0)
        self.variable = tk.StringVar(self)
        self.variable.set("0")
        self.option = tk.OptionMenu(self, self.variable, "0", "1", "2", "3").grid(column=3, row=1)            


    def plot(self):
        
        self.fig.clear()
        self.t = None
        
        #Gets dt from input box
        self.dt = float(self.dt_entry.get())
        #Creates self.t array from 'time' input box and dt
        self.t = np.arange(0.0, float(self.time.get()), self.dt)
    
        #Sets mass and length and friction from input from sliders      
        self.L1 = float(self.length1.get())
        self.L2 = float(self.length2.get())
        self.M1 = self.m1.get()
        self.M2 = self.m2.get()
        
        #Sets initial conditions
        th1 = 90    # initial condition for theta1
        w1 = 0      # initial condition for omega1
        th2 = 0     # initial condition for theta2
        w2 = 0      # initial condition for omega2
 
        rad = np.pi/180

        # initial state for theta1, omega1, theta2, omega2
        state = np.array([th1, w1, th2, w2])*np.pi/180.

        # integrate your ODE using scipy.integrate.
        y = integrate.odeint(self.equation, state, self.t)

        # creates arrays of points for pendulum 
        self.x1 = self.L1*np.sin(y[:,0])
        self.y1 = -self.L1*np.cos(y[:,0])

        self.x2 = self.L2*np.sin(y[:,2]) + self.x1
        self.y2 = -self.L2*np.cos(y[:,2]) + self.y1

        # create canvas for matplot
        canvas = FigureCanvasTkAgg(self.fig, master=self)
        canvas.get_tk_widget().grid(column=0, row= 8, columnspan=8)

        # create subplot for animation
        ax1 = self.fig.add_subplot(221, autoscale_on=False, xlim=(-3, 3), ylim=(-5, 1))
        ax1.set_title('Animation')
        ax1.grid()

        self.line, = ax1.plot([], [], 'o-', lw=2)
        self.line.set_data([], [])
        # create time counter for animation
        self.time_template = 'time = %.1fs'
        self.time_text = ax1.text(0.05, 0.9, '', transform=ax1.transAxes)
        self.time_text.set_text('')

        # create animation
        self.ani = animation.FuncAnimation(self.fig, self.animate, np.arange(1, len(y)),
                                      interval=25, blit=False, init_func=self.init, repeat=True)
        
        
        # create subplot for plot of variable vs. time
        ax2 = self.fig.add_subplot(222)
        ax2.plot(self.t, y[:, float(self.variable.get())])  # sets number from variable to plot
        ax2.grid()
        ax2.set_title('Selected varibale vs. Time')
       
        canvas.show()
        
        
    def init(self):
        return self.line, self.time_text

    # animates line for double pendulum
    def animate(self,i):
        self.thisx = [0, self.x1[i], self.x2[i]]
        self.thisy = [0, self.y1[i], self.y2[i]]
        self.line.set_data(self.thisx, self.thisy)
        self.time_text.set_text(self.time_template%(i*self.dt))
        return self.line, self.time_text

    # function that defines physics of double pendulum
    def equation(self, state, t):
        
        dydx = np.zeros_like(state)
        dydx[0] = state[1]

        del_ = state[2]-state[0]
        den1 = (self.M1+self.M2)*self.L1 - self.M2*self.L1*np.cos(del_)*np.cos(del_)
        dydx[1] = (self.M2*self.L1*state[1]*state[1]*np.sin(del_)*np.cos(del_)
                   + self.M2*self.g*np.sin(state[2])*np.cos(del_) + self.M2*self.L2*state[3]*state[3]*np.sin(del_)
                   - (self.M1+self.M2)*self.g*np.sin(state[0]))/den1

        dydx[2] = state[3]

        den2 = (self.L2/self.L1)*den1
        dydx[3] = (-self.M2*self.L2*state[3]*state[3]*np.sin(del_)*np.cos(del_)
                   + (self.M1+self.M2)*self.g*np.sin(state[0])*np.cos(del_)
                   - (self.M1+self.M2)*self.L1*state[1]*state[1]*np.sin(del_)
                   - (self.M1+self.M2)*self.g*np.sin(state[2]))/den2

        return dydx



root = tk.Tk()

app = Application(master=root)
app.master.title("Pendulum simulation")
app.mainloop()

root.destroy()