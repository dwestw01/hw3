# -*- coding: utf-8 -*-
"""
Created on Sat Oct 11 12:09:34 2014

@authors:

David Westwood
Greg Kline


A nice little intro GUI
"""


import Tkinter as tk

import numpy as np
import matplotlib
matplotlib.use('TkAgg')

from numpy import arange, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure


class Application(tk.Frame):

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.createWidgets()
        self.pack()
        
        #Parameters for the simulation
        self.g = -9.81 
        self.links = 2
        self.ic = np.zeros((self.links,0))
        

    def say_hi(self):
        print "hi there, everyone!"

    def show_values(self):
        print (self.w1.get(), self.w2.get())


    def createWidgets(self):

        #Create sliders       
        self.w1 = tk.Scale(self, from_=0.00, to=20.00, resolution=0.1, label = "Gravity (0 to 20g")
        self.w1.grid(column=3, row = 1)
        self.w1.set(9.8)
        self.w2 = tk.Scale(self, from_=-90.0, to=90.0, orient=tk.HORIZONTAL, resolution=0.01, label = "Starting Angle(deg)")
        self.w2.grid(column=3, row = 3)
        self.w2.set(90.0)
        
        #Create buttons                
        self.quit = tk.Button(self, text="Quit", command=self.quit).grid(column=0,row=0, pady=10)
        self.hi_there = tk.Button(self, text="Hello", command=self.say_hi).grid(column=0,row=1, pady=10)
        self.plot = tk.Button(self, text="Plot", command=self.plot).grid(column=0,row=2, pady=10)
        self.show = tk.Button(self, text='Show Sliders', command=self.show_values).grid(column=0, row=3, pady=10)        
        self.run_sim = tk.Button(self,text ='Run Simulation', command=self.run_simulation).grid(column=0, row = 4, pady = 10)
        
        #Create entry         
        self.entrythingy = tk.Entry()
        self.entrythingy.pack()
        self.mass = tk.Entry()
        self.mass.pack()
        self.length = tk.Entry()
        self.length.pack()
        
        #Create string variable
        self.dampening = tk.StringVar()
        self.masstxt = tk.StringVar()
        self.ltxt = tk.StringVar()
        
        # set it to some value
        self.dampening.set("linear damping (kg/s)")
        self.masstxt.set("Mass (kg)")
        self.ltxt.set("Length (m)")
        
        # tell the entry widget to watch this variable
        self.entrythingy["textvariable"] = self.dampening
        self.mass["textvariable"] = self.masstxt
        self.length["textvariable"] = self.ltxt
        
        # and here we get a callback when the user hits return.
        # we will have the program print out the value of the
        # application variable when the user hits return
        self.entrythingy.bind('<Key-Return>', self.print_contents)
        self.mass.bind('<Key-Return>', self.print_contents)
        self.length.bind('<Key-Return>', self.print_contents)
        
    def plot(self):
        fig = Figure(figsize=(5,4), dpi=100)
        amplitude = self.w1.get()
        freq = self.w2.get()
        a = fig.add_subplot(111)
        t = arange(0.0,3.0,0.01)
        s = amplitude*sin(2*pi*t*freq)
        a.plot(t,s)
        # a tk.DrawingArea
        canvas = FigureCanvasTkAgg(fig, master=self)
        canvas.show()
        canvas.get_tk_widget().grid(column=0, row= 15, columnspan=12)
        
    def print_contents(self, event):
        print "The contents of entry is now ---->", self.contents.get()


    #set number of links        
    def set_number_links(self):
        pass 
    
    #set the initial conditions
    def set_ic(self):
        return self.w2.get()
    
    #Set the gravity
    def set_gravity(self):
        return self.w1.get()
        
    #We can use the same function to change all the parameters (dictionary, id=>variable)
    def change_parameter(self):
        pass

    #Re-run simulation
    def rerun(self):
        pass
    
    # Function to run the forward dynamics simulation
    # if arguments are None the function will use the ic and gravity from the class
    def run_simulation(self, ic=None, gravity=None):
        x = []
        g = float(self.w1.get())
        ia = float(self.w2.get())
        m = float(self.mass.get())
        l = float(self.length.get())
        h = 1
        omega = np.array([ia,ia])
        p = np.array([0,0])
        
        for i in range(0,5):
            print"fg"
            print omega
            o = next_step(m,l,g,omega,p,h)
            x.append(o)
            omega = o[0]
            p = o[1]
        
        print x 
        
        
def next_step(m,l,g,theta,p_theta, h):
    theta_dot = np.array([0,0])
    p_theta_dot = np.array([0,0])

    theta_dot[0] = 6/(m*l**2)*(2*p_theta[0] - 3*np.cos(theta[0] - theta[1])*p_theta[1])/(16 - 9*np.cos(theta[0]-theta[1])**2)
    theta_dot[1] = 6/(m*l**2)*(8*p_theta[1] - 3*np.cos(theta[0] - theta[1])*p_theta[0])/(16 - 9*np.cos(theta[0]-theta[1])**2)

    p_theta_dot[0] = .5*m*l**2*(theta_dot[0]*theta_dot[1]*np.sin(theta[0]-theta[1])+3*g/l*np.sin(theta[0]))
    p_theta_dot[1] = .5*m*l**2*(-theta_dot[0]*theta_dot[1]*np.sin(theta[0]-theta[1])+g/l*np.sin(theta[1]))
    
    p = p_theta + p_theta_dot*h
    o = theta + theta_dot*h
    
    x = np.array([o, p])
    print "x"
    print x
    print "td"
    print theta_dot
    print "pd"
    print p_theta_dot
    
    return x
    #x = [theta, p_theta]
    #xdot = [theta_dot, p_theta_dot]
    
#rk 4 method

    
root = tk.Tk()

app = Application(master=root)
app.master.title("Homework 3")
app.mainloop()

root.destroy()

